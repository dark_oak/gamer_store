<?php

include '../Model/CompraProduto.php';

class ControllerCompraProduto
{
  public function exibeAllCarrinho()
  {
    $modelCarrinho = new Carrinho();
    $carrinho = $modelCarrinho->getAll();
    foreach ($carrinho as $produto) {
      echo $produto['id_carrinho'] . ' ' . $produto['id_produto'] . ' ' . $produto['valor_total'] . ' ' . $produto['quantidade'];
      echo "<br>";
    }
  }

  public function inserirProdutoCarrinho()
  {
    if(isset($_POST['inserir']))
    {
      $modelCarrinho = new Carrinho();
      $id_produto = $_POST['id_produto'];
      $valor_unidade = $_POST['valor'];
      $quantidade = $_POST['quantidade'];
      $id_usuario= $_SESSION['id_usuario'];
      $modelCarrinho->insertCarrinho($id_produto, $valor_unidade, $quantidade, $id_usuario);
      header('Location: index.php?action=exibeProdutos.php');
    }
  }

  public function exibeCarrinho()
  {
    $modelCarrinho = new Carrinho();
    $carrinho = $modelCarrinho->getCarrinhoUsuario($_SESSION['id_usuario']);
    $totalGeral = 0;
    echo '<table class="table">';
    echo '<thead><th>Produto</th><th>Preço</th><th>Quantidade</th><th>Total</th><th style="width:1%"> </th></thead><tbody>';
    foreach ($carrinho as $produto) {
      echo "<tr>";
      echo "<td>";
      echo $produto['nome'];
      echo "</td>";
      echo "<td>";
      echo 'R$ ' . $produto['valor'] . '.00';
      echo "</td>";
      echo "<td>";
      echo $produto['quantidade'];
      echo "</td>";
      echo "<td>";
      echo 'R$ ' . $produto['quantidade'] * $produto['valor'] . '.00';
      echo "</td>";
      echo '<td style="width: 1%">';
      echo '<form method="post" action="editarCarrinho.php">
      <button type="submit" class="btn btn-sm btn-outline-danger" name="removeProduto"><i class="fas fa-times-circle"></i></button>
      <input type="hidden" name="id_carrinho" value="'. $produto['id_carrinho'] . '">
      </form>';
      echo "</td>";
      echo "</tr>";
      $totalGeral += $produto['quantidade'] * $produto['valor'];
    }
    echo "<tr style='color: #336699'><th>Total Geral: </th><th></th><th></th><th>R$ $totalGeral.00</th><th></th></tr>";
    echo '</tbody></table>';
  }

  public function exibeCarrinhoConsulta()
  {
    $modelCarrinho = new Carrinho();
    $carrinho = $modelCarrinho->getCarrinhoUsuario($_SESSION['id_usuario']);
    $totalGeral = 0;
    echo '<table class="table">';
    echo '<thead><th>Produto</th><th>Preço</th><th>Quantidade</th><th>Total</th></thead><tbody>';
    foreach ($carrinho as $produto) {
      echo "<tr>";
      echo "<td>";
      echo $produto['nome'];
      echo "</td>";
      echo "<td>";
      echo 'R$ ' . $produto['valor'] . '.00';
      echo "</td>";
      echo "<td>";
      echo $produto['quantidade'];
      echo "</td>";
      echo "<td>";
      echo 'R$ ' . $produto['quantidade'] * $produto['valor'] . '.00';
      echo "</td>";
      echo "</tr>";
      $totalGeral += $produto['quantidade'] * $produto['valor'];
    }
    echo "<tr style='color: #336699'><th>Total Geral: </th><th></th><th></th><th>R$ $totalGeral.00</th></tr>";
    echo '</tbody></table>';
  }

  public function removeProduto()
  {
    if (isset($_POST['removeProduto'])) {
      $modelCarrinho = new Carrinho();
      $modelCarrinho->removeCarrinhoById($_POST['id_carrinho']);
      header('Location: index.php?action=editarCarrinho.php');
    }
  }
}

 ?>
