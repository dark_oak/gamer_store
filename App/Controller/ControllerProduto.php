<?php

include '../Model/Produto.php';

class ControllerProduto
{

  public function cadastrarProduto()
  {
    if (isset($_POST['cadastrar'])) {
      $nome = $_POST['nome'];
      $quantidade = $_POST['quantidade'];
      $preco = $_POST['preco'];
      $categoria = $_POST['categoria'];
      $modelProduto = new Produto();
      $modelProduto->insertProduto($nome, $quantidade, $preco, $categoria);
    }
  }

  public function getAllProduto()
  {
    $modelProduto = new Produto();
    $produtos = $modelProduto->getAll();
    echo '<table class="table" id="produtos"><thead><th> </th><th>Nome</th><th>Quantidade Disponível</th><th>Preço</th><th>Comprar</th></thead><tbody>';
    foreach ($produtos as $produto) {
      echo '<tr>';
      echo "<td style='width: 1%'>";
      switch ($produto['id_categoria']) {
        case 1:
        {
          echo '<i class="fas fa-mouse-pointer"></i>';
          break;
        }
        case 2:
        {
          echo '<i class="fas fa-tablet"></i>';
          break;
        }
        case 3:
        {
          echo '<i class="fas fa-keyboard"></i>';
          break;
        }
        case 4:
        {
          echo '<i class="fas fa-headphones-alt"></i>';
          break;
        }
        case 5:
        {
          echo '<i class="fas fa-headset"></i>';
          break;
        }
        case 6:
        {
          echo '<i class="fab fa-elementor"></i>';
          break;
        }
        case 7:
        {
          echo '<i class="fas fa-microchip"></i>';
          break;
        }
        case 8:
        {
          echo '<i class="fas fa-memory"></i>';
          break;
        }
        case 9:
          echo '<i class="fas fa-desktop"></i>';
          break;

        default:
          break;
      }
      echo "</td>";
      echo '<td>'. $produto['nome'] .'</td><td>'. $produto['quantidade'] .'</td><td>R$ '. $produto['valor'] .'</td>';
      echo '<th style="width: 10%"><button type="button" class="btn btn-sm btn-dark" data-toggle="modal" data-target="#modal' . $produto['id_produto'] .'">
              Comprar
            </button>
            <div class="modal fade" id="modal' . $produto['id_produto'] .'" tabindex="-1" role="dialog" aria-labelledby="modal' . $produto['id_produto'] .'" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="modal' . $produto['id_produto'] .'Title">'. $produto['nome'] .'</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">

              <div class="row text-center" id="quantidade_produto' . $produto['id_produto'] .'">
                <span>&nbsp;&nbsp;Quantidade: &nbsp;</span>
                <input type="button" class="btn btn-sm btn-dark" id="plus" value=\'-\' onclick="proccess(-1, \'' . $produto['id_produto'] .'\', ' . $produto['quantidade'] . '); calcularPreco('. $produto['valor'] .', \'quant' . $produto['id_produto'] .'\', \'preco' . $produto['id_produto'] .'\')" />
                <input class="form-control  form-control-sm col-1" id="quant' . $produto['id_produto'] .'" name="quant' . $produto['id_produto'] .'" class="text" size="1" type="text" value="1" maxlength="5" size="2" disabled/>
                <input type="button" class="btn btn-sm btn-dark" id="minus" value=\'+\' onclick="proccess(1, \'' . $produto['id_produto'] .'\', ' . $produto['quantidade'] . '); calcularPreco('. $produto['valor'] .', \'quant' . $produto['id_produto'] .'\', \'preco' . $produto['id_produto'] .'\')">
              </div>
              <br>
              Total: <span style="color: black;" id="preco' . $produto['id_produto'] .'" value="1">R$ '. $produto['valor'] .' </span>

            </div>
            <div class="modal-footer">
            <form method="post" action="index.php?action=exibeProdutos.php">
              <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="submit" name="inserir" class="btn btn-sm btn-dark">Confirmar</button>
              <input type="hidden" name="quantidade" id="quantidade' . $produto['id_produto'] .'" value="1">
              <input type="hidden" name="id_produto" value="' . $produto['id_produto'] .'">
              <input type="hidden" name="valor" value="' . $produto['valor'] .'">
            </form>
            </div>
            </div>
            </div>
            </div></th>';
      echo '</tr>';
    }
    echo '</tbody></table>';
  }

  public function getAllProdutoAdmin()
  {
    $modelProduto = new Produto();
    // $produtos = $modelProduto->getAll();
    $produtos = $modelProduto->getAllProdutoAtivo();
    echo '<table class="table" id="produtos">
    <thead>
    <th> </th>
    <th>Nome</th>
    <th>Quantidade Disponível</th>
    <th>Preço</th>
    <th>Editar</th>
    <th>Excluir</th>
    </thead>
    <tbody>';
    foreach ($produtos as $produto) {
      echo '<tr>';
      echo "<td style='width: 1%'>";
      switch ($produto['id_categoria']) {
        case 1:
        {
          echo '<i class="fas fa-mouse-pointer"></i>';
          break;
        }
        case 2:
        {
          echo '<i class="fas fa-tablet"></i>';
          break;
        }
        case 3:
        {
          echo '<i class="fas fa-keyboard"></i>';
          break;
        }
        case 4:
        {
          echo '<i class="fas fa-headphones-alt"></i>';
          break;
        }
        case 5:
        {
          echo '<i class="fas fa-headset"></i>';
          break;
        }
        case 6:
        {
          echo '<i class="fab fa-elementor"></i>';
          break;
        }
        case 7:
        {
          echo '<i class="fas fa-microchip"></i>';
          break;
        }
        case 8:
        {
          echo '<i class="fas fa-memory"></i>';
          break;
        }
        case 9:
          echo '<i class="fas fa-desktop"></i>';
          break;

        default:
          break;
      }
      echo "</td>";
      echo "<td>{$produto['nome']}</td>";
      echo "<td>{$produto['quantidade']}</td>";
      echo "<td>R$ {$produto['valor']}</td>";
      echo "<td style='width: 1%'><a href='index.php?action=admin/editarProduto.php&id_produto={$produto['id_produto']}' class='btn btn-sm btn-outline-secondary'><i class='fas fa-edit'></i></a></td>";
      echo "<td style='width: 1%'><form method='post' action='index.php?action=admin/consultarProdutos.php'>
      <button type='submit' class='btn btn-sm btn-outline-danger' name='apagar'><i class='fas fa-times-circle'></i></button>
        <input type='hidden' name='id_produto' value='{$produto['id_produto']}'>
      </form>
      </td>";
      echo '</tr>';
    }
    echo '</tbody></table>';
  }

  public function removeProduto()
  {
    if(isset($_POST['apagar'])){
      $modelProduto = new Produto();
      $modelProduto->deleteProduto($_POST['id_produto']);
    }
  }

  public function editarProduto()
  {
    if(isset($_POST['editar'])) {
      $modelProduto = new Produto();
      $modelProduto->updateProduto($_POST['id_produto'], $_POST['nome'], $_POST['preco'], $_POST['quantidade'], $_POST['categoria']);
      header('Location:  index.php?action=admin/consultarProdutos.php');
    }
  }
}
