<?php

include_once '../../Core/Model.php';

class Compra extends Model
{
  public static function getAll()
  {
      $db = static::getDB();
      $stmt = $db->query('SELECT * FROM compra');
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function insertCompra($id_usuario)
  {
    $db = static::getDB();
    date_default_timezone_set('America/Sao_Paulo');
    $date = date('Y-m-d H:i');
    echo $date;

    $stmt = $db->query("INSERT INTO compra (id_usuario, data_compra) VALUES
        ($id_usuario,
          '$date')");
    return true;
  }

  public function selectCompraUsuarioNaoFinalizada($id_usuario)
  {
    $db = static::getDB();
    $stmt = $db->query("SELECT * FROM compra where id_usuario = $id_usuario AND finalizada = 0");
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function finalizarCompra($id_compra)
  {
    $db = static::getDB();
    $stmt = $db->query("UPDATE compra SET  finalizada = 1 WHERE id_compra = $id_compra");
    return true;
  }

  public function selectAllCompra(){
    $db = static::getDB();
    $stmt = $db->query("SELECT c.id_compra, c.data_compra, cp.qtd_comprada, u.nome as nome_usuario, p.nome as nome_produto
      FROM compra c
      JOIN compra_produto cp USING (id_compra)
      JOIN usuario u USING (id_usuario)
      JOIN produto p USING (id_produto)
      WHERE finalizada = 1");
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function selectAllCompraByUsuario($id_usuario)
  {
    $db = static::getDB();
    $stmt = $db->query("SELECT c.id_compra, c.data_compra, cp.qtd_comprada, u.nome as nome_usuario, p.nome as nome_produto
      FROM compra c
      JOIN compra_produto cp USING (id_compra)
      JOIN usuario u USING (id_usuario)
      JOIN produto p USING (id_produto)
      WHERE finalizada = 1
      AND id_usuario = $id_usuario");
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }
}
