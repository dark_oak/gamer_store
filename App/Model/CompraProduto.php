<?php

include_once '../../Core/Model.php';

class CompraProduto extends Model
{
  public static function getAll()
  {
      $db = static::getDB();
      $stmt = $db->query('SELECT * FROM compra_produto');
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public static function insertCompraProduto($id_compra, $id_produto, $quantidade)
  {
    $db = static::getDB();
    $stmt = $db->query("INSERT INTO compra_produto VALUES(default, $id_compra, $id_produto, $quantidade)");
    $stmt = $db->query("UPDATE produto SET quantidade = quantidade - $quantidade WHERE id_produto = $id_produto");
    return true;
  }

  public function deleteCompraProdutoById($id_compra_produto)
  {
    $db = static::getDB();
    $stmt = $db->query("SELECT id_produto, qtd_comprada FROM compra_produto WHERE id_compra_produto = $id_compra_produto");
    $result =  $stmt->fetch();

    $id_produto = $result['id_produto'];
    $qtd_comprada = $result['qtd_comprada'];

    $stmt = $db->query("DELETE FROM compra_produto WHERE id_compra_produto = $id_compra_produto");
    $stmt = $db->query("UPDATE produto SET quantidade = quantidade + $qtd_comprada WHERE id_produto = $id_produto");
    return true;
  }

  public function selectCompraNaoFinalizadaByUsuario($id_usuario)
  {
    $db = static::getDB();
    $stmt = $db->query("SELECT c.id_compra, c.data_compra, cp.id_compra_produto, cp.id_produto, cp.qtd_comprada, p.nome, p.valor, p.id_categoria
      FROM compra c
      JOIN compra_produto cp USING (id_compra)
      JOIN produto p USING (id_produto)
      WHERE c.id_usuario =  $id_usuario AND c.finalizada = 0");
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

}
