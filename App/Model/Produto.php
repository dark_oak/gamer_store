<?php

include_once '../../Core/Model.php';

class Produto extends Model
{
  public static function getAll()
  {
      $db = static::getDB();
      $stmt = $db->query('SELECT * FROM produto');
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function getAllProdutoAtivo()
  {
    $db = static::getDB();
    $stmt = $db->query('SELECT * FROM produto WHERE produto.ativo = 1');
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function selectProdutoById($id_produto)
  {
    $db = static::getDB();
    $stmt = $db->query("SELECT * FROM produto WHERE id_produto = $id_produto");
    return $stmt->fetch();
  }

  public function insertProduto($nome, $quantidade, $preco, $categoria)
  {
    $db = static::getDB();
    $stmt = $db->query("INSERT INTO produto(nome, quantidade, valor, id_categoria)
                  VALUES ('$nome', $quantidade, '$preco', $categoria)");
    return true;
  }

  public function deleteProduto($id_produto)
  {
    $db = static::getDB();
    $stmt = $db->query("UPDATE produto SET ativo = 0 WHERE id_produto = $id_produto");
    return true;
  }

  public function updateProduto($id_produto, $nome, $valor, $quantidade, $categoria)
  {
    $db = static::getDB();
    $stmt = $db->query("UPDATE produto SET nome = '$nome', valor = $valor, quantidade = $quantidade, id_categoria = $categoria WHERE id_produto = $id_produto");
    return true;
  }

}
