<?php
//admin


include_once '../Controller/ControllerUsuario.php';
include_once '../Controller/ControllerPerfil.php';

$controllerUsuario = new ControllerUsuario();
$controllerPerfil = new ControllerPerfil();

$controllerUsuario->cadastrarUsuario();

 ?>
 <div class="container">
   <table style="width: 100%">
     <th style="width: 25%">
     </th>
     <th>
   <div class="row">
     <h3>Cadastrar Usuario</h3>
   </div>
   <hr>
       <form method="post" action="index.php?action=admin/cadastrarUsuario.php">
         <div class="row">
           <label class="col-2" for="nome">Nome</label><input class="col-10 form-control" type="text" name="nome" maxlength="50" required><br>
         </div>
         <br>
         <div class="row">
           <label class="col-2" for="cpf">CPF</label><input class="col-10 form-control" type="text" name="cpf" maxlength="11" required><br>
         </div>
         <br>
         <div class="row">
           <label class="col-2" for="endereco">Endereço</label><input class="col-10  form-control" type="text" name="endereco" maxlength="100" required><br>
         </div>
         <br>
         <div class="row">
           <label class="col-2" for="email">Email</label><input class="col-10 form-control" type="text" name="email" maxlength="50" required><br>
         </div>
         <br>
         <div class="row">
           <label class="col-2" for="senha">Senha</label><input class="col-10 form-control" type="text" name="senha" maxlength="20" required><br>
         </div>
         <br>
         <div class="row">
           <label class="col-2" for="email">Perfil</label>
           <?php $controllerPerfil->getSelectOptionPerfil() ?>
         </div>
         <br>
         <div class="text-center">
           <input class="btn btn-sm btn-dark" style="width: 150px"type="submit" name="inserir" value="Cadastrar">
         </div>
       </form>
     </div>
   </div>
   <hr>
 </th>
 <th style="width: 25%">
 </th>

 </table>
 </div>

 <div class="text-center">
   <a href="index.php?action=admin/index_admin.php" class="btn btn-sm btn-warning" value='Voltar'><i class="fas fa-angle-double-left"></i> Voltar</a>
 </div>
