<?php
include 'templates/Head.php';
require_once '../Controller/ControllerProduto.php';
$controllerProduto = new ControllerProduto();

$controllerProduto->removeProduto();

?>

<div class="container">
  <?php $controllerProduto->getAllProdutoAdmin() ?>
</div>

  <div class="fixed-bottom p-2">
    <div class="text-right">
      <a href="index.php?action=admin/index_admin.php" class="btn btn-sm btn-warning" value='Voltar'><i class="fas fa-angle-double-left"></i> Voltar</a>
    </div>
</div>
