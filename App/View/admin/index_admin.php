<?php
include 'templates/Head.php';
?>

<div class="container">
  <div class="fixed-top" style="top: 4.5em; right: 2em;">
    <a class="btn btn-sm btn-outline-dark float-right" href="index.php?action=sobre.php"><i class="fas fa-info-circle"></i> Sobre</a>
  </div>

  <div class="row">
    <h3>Usuário</h3>
  </div>
  <hr>
  <a href="index.php?action=admin/consultarUsuarios.php" style="color: #343a40; text-decoration-line: underline">Listar Usuários</a><br>
  <a href="index.php?action=admin/cadastrarUsuario.php" style="color: #343a40; text-decoration-line: underline">Cadastrar Usuário</a>

<br>
<br>
  <div class="row">
    <h3>Produto</h3>
  </div>
  <hr>

  <a href="index.php?action=admin/cadastrarProduto.php" style="color: #343a40; text-decoration-line: underline">Cadastrar Produto</a><br>
  <a href="index.php?action=admin/consultarProdutos.php" style="color: #343a40; text-decoration-line: underline">Consultar Produto</a>

  <br>
  <br>
    <div class="row">
      <h3>Compra</h3>
    </div>
    <hr>

    <a href="index.php?action=admin/vizualizarCompras.php" style="color: #343a40; text-decoration-line: underline">Vizualizar Compras</a>
</div>

<hr>
<div class="text-center">
  <a href="index.php?action=exibeProdutos.php" class="btn btn-sm btn-warning" value='Voltar'><i class="fas fa-angle-double-left"></i>Voltar</a>
</div>

<script type="text/javascript" src="styles/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="styles/js/bootstrap.js"/></script>
