<?php
include_once '../Controller/ControllerProduto.php';
include_once '../Controller/ControllerCompra.php';

$controllerProduto = new ControllerProduto();
$controllerCompra = new ControllerCompra();

$controllerCompra->inserirProdutoEmCompra();

?>

<div class="container">


<?php
$controllerProduto->getAllProduto();
 ?>

</div>

<script type="text/javascript" src="styles/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="styles/js/datatables.js"/></script>
<script type="text/javascript" src="styles/js/bootstrap.js"/></script>

<script>
$(document).ready( function () {
  $('#produtos').DataTable({
    "dom": 'flrtip',
    "language": {
      "sEmptyTable": "Nenhum registro encontrado",
      "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
      "sInfoFiltered": "(Filtrados de _MAX_ registros)",
      "sInfoPostFix": "",
      "sInfoThousands": ".",
      "sLengthMenu": "_MENU_ resultados por página",
      "sLoadingRecords": "Carregando...",
      "sProcessing": "Processando...",
      "sZeroRecords": "Nenhum registro encontrado",
      "sSearch": "Pesquisar",
      "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
      },
    }
} );
});


function proccess(quant, id, limit){
    var value = parseInt(document.getElementById('quant' + id).value);
    value+=quant;
    if(value < 1){
      document.getElementById('quant' + id).value = 1;
      document.getElementById('quantidade' + id).value = 1;
    } else if (limit && value > limit) {
      document.getElementById('quant' + id).value = parseInt(limit);
      document.getElementById('quantidade' + id).value = parseInt(limit);
    } else{
    document.getElementById('quant' + id).value = value;
    document.getElementById('quantidade' + id).value = value;
    }
    console.log(document.getElementById('quantidade' + id).value + " - " + limit);
  }

function calcularPreco(preco, id, alterado) {
  var quantidade = parseInt(document.getElementById(id).value);
  var total = preco * quantidade;
  total = parseFloat(total.toFixed(3));

  document.getElementById(alterado).innerHTML = "R$ " + total;
}
</script>
