<div class="container">
  <h3>Diagrama ER</h3>
  <hr>
  <img src="styles/img/gamer_store.png" class="rounded mx-auto d-block" alt="Diagrama ER">
  <h3>Especificações</h3>
  <hr>
  <ul>
    <li>Linguagem de Programação: PHP7</li>
    <li>Banco de Dados: MySQL</li>
    <li>Arquitetura: MVC</li>
  </ul>
  <h4>Bibliotecas Externas Utilizadas</h4>
  <ul>
    <li>FontAwesome v5.5.0 </li>
    <li>Bootstrap v4.1.3</li>
    <li>DataTables v1.10.19</li>
  </ul>
</div>

<div class="text-center">
  <a href="index.php?action=admin/index_admin.php" class="btn btn-sm btn-warning" value='Voltar'><i class="fas fa-angle-double-left"></i> Voltar</a>
</div>
