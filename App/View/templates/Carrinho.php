<div class="modal fade" id="carrinho" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Carrinho : <?php echo $_SESSION['nome'] ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php $controllerCompra->exibeCompraNaoFinalizadaConsulta() ?>
      </div>
      <div class="modal-footer">
        <a href="index.php?action=editarCarrinho.php" class="btn btn-sm btn-outline-dark">Ver Carrinho</a>
        <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
