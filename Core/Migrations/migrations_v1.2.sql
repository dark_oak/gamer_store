create database gamers_club
default charset utf8
default collate utf8_general_ci;

use gamers_club;

create table perfil(
  id_perfil int(11) auto_increment not null,
  descricao varchar(50),
  PRIMARY KEY(id_perfil)
)default charset utf8;

create table usuario(
  id_usuario int not null auto_increment,
  nome varchar(50) not null,
  cpf bigint(11) not null,
  email varchar(50),
  senha varchar(20),
  endereco varchar(100),
  id_perfil int(11) not null default 2,
  primary key (id_usuario),
  foreign key (id_perfil) references perfil(id_perfil)
)default charset utf8;

create table categoria(
  id_categoria int(11) not null auto_increment,
  descricao varchar(40),
  primary key (id_categoria)
)default charset utf8;

create table produto(
  id_produto int(11) not null auto_increment,
  nome varchar(50) not null,
  quantidade int(11) not null,
  valor float(7,2),
  id_categoria int(11),
  primary key(id_produto),
  foreign key(id_categoria) references categoria(id_categoria)
)default charset utf8;

create table carrinho(
  id_carrinho int not null auto_increment,
  id_produto int(11),
  valor_unidadade float(7,2) not null default 0,
  quantidade int(11) not null default 0,
  id_usuario int(11) not null,
  primary key (id_carrinho),
  foreign key (id_usuario) references usuario(id_usuario),
  foreign key (id_produto) references produto(id_produto)
)default charset utf8;

create table compra(
  id_compra int(11) not null auto_increment,
  quantidade int(11),
  valor_unidade int(11),
  id_produto int(11),
  id_usuario int(11),
  primary key(id_compra),
  foreign key(id_produto) references produto(id_produto),
  foreign key(id_usuario) references usuario(id_usuario)
)default charset utf8;

insert into categoria values
  (1, 'Mouse'),
  (2, 'Processador'),
  (3, 'Fone'),
  (4, 'Headset');

insert into perfil values
  (1, 'ADMIN'),
  (2, 'CLIENTE');

insert into usuario values
  (default, 'Brayan Ferreira', 99988877743, 'brayan@email.com', 'ne', '16', 1),
  (default, 'Gustavo "Rei Delas" Carvalho', 11122233365, 'gustavo@email.com', 'batata', '5', 2);

insert into produto values
(default, 'Deathadder elite', 56, '350,00', 1),
(default, 'Pulsefire FPS', 23, '170,00', 1),
(default, 'Lobo', 77, '269,90', 1),
(default, 'Cobra', 11, '160,00', 1),
(default, 'G402', 54, '100,00', 1),
(default, 'G502', 33, '150,00', 1),
(default, 'G403', 76, '170,00', 1),
(default, 'EC1-A', 44, '438,90', 1),
(default, 'EC1-B', 44, '429,90', 1),
(default, 'G4560', 30, '230,00', 2),
(default, 'I3', 100, '850,00', 2),
(default, 'I5', 100, '1250,00', 2),
(default, 'I7', 100, '1800,00', 2),
(default, 'I9', 100, '9000,00', 2),
(default, 'FX8370E', 11, '459,90', 2),
(default, 'FX8300', 34, '349,90', 2),
(default, 'Rysen 5 1600X', 50, '799,90', 2),
(default, 'Rysen 5 2600', 29, '829,90', 2),
(default, 'Rysen 7 1700', 30, '925,00', 2),
(default, 'Hammerhead Pro v2', 55, '624,90', 3),
(default, 'EarBuds', 100, '279,90', 3),
(default, 'Cloud Stinger', 33, '250,00', 4),
(default, 'Cloud 2', 100, '450,00', 4),
(default, 'Cloud Alpha', 35, '550,00', 4),
(default, 'Revolver', 45, '650,00', 4),
(default, 'Revolver S', 34, '850,00', 4),
(default, 'Kraken', 9, '200,00', 4),
(default, 'Kraken V2', 17, '350', 4),
(default, 'Electra', 11, '372,00', 4),
(default, 'G231', 17, '180,00', 4),
(default, 'G233', 23, '289,90', 4),
(default, 'G430', 78, '299,90', 4),
(default, 'G633', 24, '594,90', 4),
(default, 'GSP 600', 99, '1749,99', 4),
(default, 'GSP 500', 12, '1609,99', 4),
(default, 'Game Zero', 54, '1269,99', 4),
(default, 'GSP 300', 34, '281,45', 4)
;
